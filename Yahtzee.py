import random
outcomes = [random.randint(1,6) for i in range(5)]

def sequence():
  return sorted(outcomes)
print(sequence())

def summation():
  return sum(outcomes)
print(summation())

def NumberKind():
  return max(list(outcomes.count(i) for i in outcomes))
print(NumberKind())

def FiveOfAKind():
  if NumberKind() == 5:
    return 50
  return 0
print(FiveOfAKind())

def FourOfAKind():
  if NumberKind() == 4:
    return summation()
  return 0
print(FourOfAKind())

def ThreeOfAKind():
  if NumberKind() == 3:
    return summation()
  return 0
print(ThreeOfAKind())

def FullHouse():
  if summation() == 25:
    return 25
  return 0
print(FullHouse())

def SMStraight():
  dice = [str(x) for x in sequence()]
  possibilities = ["1234","2345","3456"]
  for i in possibilities:
    if i in ("".join(dice)):
      return True
  return False
print(SMStraight())

def LMStraight():
  dice = [str(x) for x in sequence()]
  possibilities = ["12345","23456"]
  for i in possibilities:
    if i in ("".join(dice)):
      return True
  return False
print(LMStraight())

def UpperSection():
    slot_score = max(set(outcomes), key = outcomes.count)
    return (slot_score *(outcomes.count(slot_score)))
print(UpperSection())

def Chance():
  return summation()
print(Chance())
