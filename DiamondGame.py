import random
T = 10
J = 11
Q = 12
K = 13
A = 14
DIAMONDS = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]
random.shuffle(DIAMONDS)
HEARTS = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]
random.shuffle(HEARTS)
CLUBS = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]
def strategy(diamonds,HEARTS):
  if diamonds == "K" :
    return " ".join(str(x) for x in [i for i in HEARTS if eval(i) == max([eval(i) for i in HEARTS])])
  elif diamonds == "A" or diamonds == "2" or diamonds == "3" or diamonds == "4" or diamonds == "5" or diamonds == "6" :
    return " ".join(str(x) for x in [i for i in HEARTS if eval(i) == min([eval(i) for i in HEARTS])])
  else:
    return " ".join(str(x) for x in [i for i in HEARTS if eval(i) == eval(diamonds) + 1])
def diamond_game():
  count = 0
  user_score = 0
  computer_score = 0
  while count < 13:
    diamonds = DIAMONDS[count]
    print("Diamonds: ",diamonds)
    computer = strategy(diamonds,HEARTS)
    HEARTS.remove(computer)
    user = str(input("User bid : "))
    if user in CLUBS:
      CLUBS.remove(user)
      print("Remaining cards in user suit: ",CLUBS)
    else:
      print("Input is already given")
      break
    print("Computer bid : ",computer)
    count += 1
    if eval(user) > eval(computer):
      user_score = user_score + eval(diamonds)
    elif eval(user) < eval(computer):
      computer_score = computer_score + eval(diamonds)
    elif eval(user) == eval(computer):
      computer_score = computer_score + eval(diamonds)/2
      user_score = user_score + eval(diamonds)/2
    else:
      continue
    print("user score: ",user_score)
    print("computer score: ",computer_score)
    print("=====================================")
    if count >= 13:
      if user_score > computer_score:
        return "User won"
      else:
        return "Computer won"
      
print(diamond_game())
