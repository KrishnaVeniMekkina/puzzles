from array import *


#Passing list
lis = []
for i in range(5):
    x = [j for j in input()]
    lis.append(x)
moves = [x for x in input()]
r = 0
c = 0
#Passing array
for i in range(len(lis)):
    for j in range(len(lis[i])):
        if lis[i][j] == ' ':
            r += i
            c += j
    if c == j:
        break
#Defining the moves
for x in moves:
    if x == 'A':
        lis[r][c] = lis[r-1][c]
        lis[r-1][c] = ' '
        r = r-1
    elif x == 'B':
        lis[r][c] = lis[r+1][c]
        lis[r+1][c] = ' '
        r = r+1
    elif x == 'R':
        lis[r][c] = lis[r][c+1]
        lis[r][c+1] = ' '
        c = c+1
    elif x == 'L':
        lis[r][c] = lis[r][c-1]
        lis[r][c-1] = ' '
        c = c-1
    elif x == 'Z':
        print("This data has no final confiration")

#Printing result
res = []
for i in range(len(lis)):
    x = " ".join(lis[i])
    res.append(x)

for y in res:
    print(y) 